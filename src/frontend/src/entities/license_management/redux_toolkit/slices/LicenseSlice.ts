/*
 *  Copyright (C) <2023>  <becon GmbH>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {ICommonState} from "@application/interfaces/core";
import {API_REQUEST_STATE, TRIPLET_STATE} from "@application/interfaces/IApplication";
import {CommonState} from "@application/utils/store";
import {IResponse} from "@application/requests/interfaces/IResponse";
import {
    activateFreeLicense,
    activateLicenseFile,
    activateLicenseString,
    deleteLicense,
    generateActivateRequest,
    getActivationRequestStatus,
    getLicenseList,
    getLicenseStatus,
} from "@entity/license_management/redux_toolkit/action_creators/LicenseCreators";
import LicenseModel, {
    ActivationRequestStatus,
    LicenseListItem
} from "@entity/license_management/requests/models/LicenseModel";
import {StatusResponse} from "@application/requests/interfaces/IApplication";

export interface LicenseState extends ICommonState{
    generatingActivateRequest: API_REQUEST_STATE,
    activatingLicense: API_REQUEST_STATE,
    gettingLicenseStatus: API_REQUEST_STATE,
    gettingActivationRequestStatus: API_REQUEST_STATE,
    gettingLicenseList: API_REQUEST_STATE,
    deletingLicense: API_REQUEST_STATE,
    activatingFreeLicense: API_REQUEST_STATE,
    license: LicenseModel,
    licenseList: LicenseListItem[],
    status: TRIPLET_STATE,
    activationRequestStatus: ActivationRequestStatus,
}

const initialState: LicenseState = {
    generatingActivateRequest: API_REQUEST_STATE.INITIAL,
    activatingLicense: API_REQUEST_STATE.INITIAL,
    gettingLicenseStatus: API_REQUEST_STATE.INITIAL,
    gettingActivationRequestStatus: API_REQUEST_STATE.INITIAL,
    deletingLicense: API_REQUEST_STATE.INITIAL,
    gettingLicenseList: API_REQUEST_STATE.INITIAL,
    activatingFreeLicense: API_REQUEST_STATE.INITIAL,
    licenseList: [],
    license: null,
    status: TRIPLET_STATE.INITIAL,
    activationRequestStatus: ActivationRequestStatus.EXPIRED,
    ...CommonState,
}

export const licenseSlice = createSlice({
    name: 'license',
    initialState,
    reducers: {
    },
    extraReducers: {
        [getLicenseList.pending.type]: (state) => {
            state.gettingLicenseList = API_REQUEST_STATE.START;
        },
        [getLicenseList.fulfilled.type]: (state, action: PayloadAction<LicenseListItem[]>) => {
            state.gettingLicenseList = API_REQUEST_STATE.FINISH;
            state.licenseList = action.payload;
            state.error = null;
        },
        [getLicenseList.rejected.type]: (state, action: PayloadAction<IResponse>) => {
            state.gettingLicenseList = API_REQUEST_STATE.ERROR;
            state.error = action.payload;
        },
        [generateActivateRequest.pending.type]: (state) => {
            state.generatingActivateRequest = API_REQUEST_STATE.START;
        },
        [generateActivateRequest.fulfilled.type]: (state, action: PayloadAction<LicenseModel>) => {
            state.generatingActivateRequest = API_REQUEST_STATE.FINISH;
            state.license = action.payload;
            state.activationRequestStatus = ActivationRequestStatus.PENDING;
            state.error = null;
        },
        [generateActivateRequest.rejected.type]: (state, action: PayloadAction<IResponse>) => {
            state.generatingActivateRequest = API_REQUEST_STATE.ERROR;
            state.error = action.payload;
        },
        [activateLicenseString.pending.type]: (state) => {
            state.activatingLicense = API_REQUEST_STATE.START;
        },
        [activateLicenseString.fulfilled.type]: (state, action: PayloadAction<LicenseModel>) => {
            state.activatingLicense = API_REQUEST_STATE.FINISH;
            state.license = action.payload;
            state.error = null;
        },
        [activateLicenseString.rejected.type]: (state, action: PayloadAction<IResponse>) => {
            state.activatingLicense = API_REQUEST_STATE.ERROR;
            state.error = action.payload;
        },
        [activateLicenseFile.pending.type]: (state) => {
            state.activatingLicense = API_REQUEST_STATE.START;
        },
        [activateLicenseFile.fulfilled.type]: (state, action: PayloadAction<LicenseModel>) => {
            state.activatingLicense = API_REQUEST_STATE.FINISH;
            state.license = action.payload;
            state.error = null;
        },
        [activateLicenseFile.rejected.type]: (state, action: PayloadAction<IResponse>) => {
            state.activatingLicense = API_REQUEST_STATE.ERROR;
            state.error = action.payload;
        },
        [getLicenseStatus.pending.type]: (state) => {
            state.gettingLicenseStatus = API_REQUEST_STATE.START;
        },
        [getLicenseStatus.fulfilled.type]: (state, action: PayloadAction<StatusResponse>) => {
            state.gettingLicenseStatus = API_REQUEST_STATE.FINISH;
            state.status = action.payload.status === 'on' ? TRIPLET_STATE.TRUE : TRIPLET_STATE.FALSE;
            state.error = null;
        },
        [getLicenseStatus.rejected.type]: (state, action: PayloadAction<IResponse>) => {
            state.gettingLicenseStatus = API_REQUEST_STATE.ERROR;
            state.error = action.payload;
            state.status = TRIPLET_STATE.FALSE;
        },
        [getActivationRequestStatus.pending.type]: (state) => {
            state.gettingActivationRequestStatus = API_REQUEST_STATE.START;
        },
        [getActivationRequestStatus.fulfilled.type]: (state, action: PayloadAction<ActivationRequestStatus>) => {
            state.gettingActivationRequestStatus = API_REQUEST_STATE.FINISH;
            state.activationRequestStatus = action.payload;
            state.error = null;
        },
        [getActivationRequestStatus.rejected.type]: (state, action: PayloadAction<IResponse>) => {
            state.gettingActivationRequestStatus = API_REQUEST_STATE.ERROR;
            state.error = action.payload;
        },
        [deleteLicense.pending.type]: (state) => {
            state.deletingLicense = API_REQUEST_STATE.START;
        },
        [deleteLicense.fulfilled.type]: (state, action: PayloadAction<ActivationRequestStatus>) => {
            state.deletingLicense = API_REQUEST_STATE.FINISH;
            state.license = null;
            state.error = null;
        },
        [deleteLicense.rejected.type]: (state, action: PayloadAction<IResponse>) => {
            state.deletingLicense = API_REQUEST_STATE.ERROR;
            state.error = action.payload;
        },
        [activateFreeLicense.pending.type]: (state) => {
            state.activatingFreeLicense = API_REQUEST_STATE.START;
        },
        [activateFreeLicense.fulfilled.type]: (state, action: PayloadAction<IResponse>) => {
            state.activatingFreeLicense = API_REQUEST_STATE.FINISH;
            state.error = null;
        },
        [activateFreeLicense.rejected.type]: (state, action: PayloadAction<IResponse>) => {
            state.activatingFreeLicense = API_REQUEST_STATE.ERROR;
            state.error = action.payload;
        },
    }
})

export default licenseSlice.reducer;
