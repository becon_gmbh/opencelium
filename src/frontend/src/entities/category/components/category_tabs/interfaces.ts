export interface CategoryTabsProps{
  readOnly?: boolean;
  setCurrentPage: (currentPage: number) => void,
}
