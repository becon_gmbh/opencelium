package com.becon.opencelium.backend.enums;

public enum ActivReqStatus {
    PENDING,
    PROCESSED,
    EXPIRED
}
