package com.becon.opencelium.backend.constant;

import com.becon.opencelium.backend.enums.ActivReqStatus;

public interface Constant {
    String CONN_FROM = "CONN1";
    String CONN_TO = "CONN2";
    String NEVER_TRIGGERED_CRON = "59 59 23 31 12 ? 2123";
}
