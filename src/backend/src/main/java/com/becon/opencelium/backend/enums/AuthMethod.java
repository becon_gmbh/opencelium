package com.becon.opencelium.backend.enums;

public enum AuthMethod {
    LDAP, BASIC
}
