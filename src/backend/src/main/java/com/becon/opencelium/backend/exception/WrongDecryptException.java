package com.becon.opencelium.backend.exception;

public class WrongDecryptException extends RuntimeException{

    public WrongDecryptException(Throwable cause) {
        super(cause);
    }
}
